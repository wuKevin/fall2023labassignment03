package dawson.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest {
    /*
     * echo test
     */
    @Test
    public void shouldReturnInt() {
        assertEquals("Test should return 5", 5, App.echo(5));
    }

    /*
     * oneMore test
     */
    @Test
    public void shouldReturnIntPlusOne() {
        assertEquals("Test should return 5+1", 6, App.echo(5));
    }
}
